let FAClient = null;
let notyf = null;
let apiKey = null;
let bingKey = null;
let deliveryLinesGlobal = null;
let recordId = null;
let recordsGlobal = false;

let routeResultGlobal = null;

let selectedWaypoints = [];

let config = {
    appName: "logo",
    locationField: "hq_location",
    nameField: "name"
}

let center = { lat: 37.7945245, lng: -122.2611597 };

let startEndLocation = "37.7945245, -122.2611597";

let directionsBaseUrl = "https://www.google.com/maps/dir/?api=1"
//let bingKey = "ApG0F4UDDJImvZJw-YRhOf0wTEUWQ3O-v4mx7ua0hiYOUhHB43UR3CEoCVc_zAa4";

// aHR0cDovL2xvY2FsaG9zdDo1MDAw

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cDovL2xvY2FsaG9zdDo1MDAw`,
};

let parseLocation = (place) => place.replace(/\s+/g, '+').replace(/,/g, '%2C');

let parseWithLabel = (waypoints) => {
    let waypointsArray = [];
    waypoints.split('|').map((wayp, index) => {
        waypointsArray.push(`markers=color:blue%7Clabel:${index + 1}%7C${parseLocation(wayp)}`);
    })
    return waypointsArray.join('&');
}

let parseTime = (time) => {
    if(time.includes(AM)) {
        return `${time.split(':')[0]}:${time.split(':')[1].substring(0,2)}:00`
    } else {
        return `${parseFloat(time.split(':')[0]) + 12}:${time.split(':')[1].substring(0,2)}:00`
    }
};

let toDataURL = (url) => {
    return fetch(url).then((response) => {
        return response.blob();
    }).then(blob => {
        return URL.createObjectURL(blob);
    });
}

let getLocation = (lonLatStr) => {
    return {
        "latitude": parseFloat(lonLatStr.split(',')[0]),
        "longitude": parseFloat(lonLatStr.split(',')[1])
    }
}


async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}


let download = async (url, name='Route') => {
    const a = document.createElement("a");
    a.href = await toDataURL(url);
    a.download = `${name}.png`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

function updateRecord(id=recordId, entity,fieldValues={}) {
    let updatePayload = {
        entity: entity,
        id: id,
        field_values: fieldValues
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}


function startupService() {
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    apiKey = FAClient.params.key;
    bingKey = FAClient.params.bingKey;

    const iFrame = document.querySelector('#directions-iframe');
    const mapContainer = document.querySelector('#map-container');

    FAClient.on('displayLocations', ({records}) => {
       // const myLatlng = { lat: 37.7945245, lng: -122.2611597 };

        //const center = new google.maps.LatLng(37.7945245, -122.2611597);
        recordsGlobal = records;

        FAClient.open();

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: center,
        });

        const geocoder = new google.maps.Geocoder();

        const svgMarker = {
            path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
            fillColor: "rgba(73, 134, 232, 1.00)",
            fillOpacity: 1,
            strokeWeight: 0,
            rotation: 0,
            scale: 1.5,
            anchor: new google.maps.Point(10, 18),
        };

        /*
        new google.maps.Marker({
            position: map.getCenter(),
            icon: svgMarker,
            map: map,
        });
         */

        records?.map(({ field_values, id }) => {
            let address = field_values[config.locationField]?.value;
            let name = field_values[config.nameField]?.value;
            setMarker({
                address: address,
                geocoder,
                map,
                isCenter: true,
                icon: null,
                title: `${id}|${name}`
            })
        })

         let getRouteButton = document.querySelector('#get-route-button');
        getRouteButton.addEventListener('click', (event) => {
            if(getRouteButton.textContent === 'Save Route') {
                createTask();
            } else {
                document.querySelector('#map').style.width = "63%";
                document.querySelector('#right-panel').style.display = "block";
                getRouteButton.textContent = 'Save Route';
                initMap(startEndLocation, startEndLocation);
            }
        })

        console.log({records})
    })


    FAClient.on('getDirections', ({record}) => {
        console.log(record)
        const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
        const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
        const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
        const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
        const originParsed = `origin=${parseLocation(startLocation)}`;
        const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');
        const destinationParsed = `destination=${parseLocation(endLocation)}`;

        let existingImg = mapContainer.querySelector('img');
        if(existingImg) {
            existingImg.remove();
        }

        recordId = record.id;
        FAClient.listEntityValues(
            {
                entity: "deliveries",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                deliveryLinesGlobal = lines;


                let locationsWithOrder = {};
                let locations = [];
                lines.map((line, index) => {
                    let order = _.get(line, `field_values[deliveries_field11].value`, null);
                    order =  order && order !== '' ? order : index + 1;
                    locationsWithOrder[order] = _.get(line, `field_values[deliveries_field2].value`, '');
                    return null;
                });

                lines.map((line, index) => {
                    locations.push(locationsWithOrder[index + 1])
                });

                console.log(locations)

                const waypointsParsed = `waypoints=${parseLocation(locations.join('|'))}`;
                let baseURL = "https://www.google.com/maps/embed/v1/directions?";
                iFrame.src = `${baseURL}&key=${apiKey}&${originParsed}&${destinationParsed}&${waypointsParsed}&mode=${travelMode}&units=${units}&maptype=${mapType}`;
                iFrame.style.display = "block";
                FAClient.open();
            });
    });


    FAClient.on('openDirections', (data) => {
        const travelMode = _.get(data, `record.field_values[map_field3].display_value`, 'driving').toLowerCase();
        const units = _.get(data, `record.field_values[map_field8].display_value`, 'metric').toLowerCase();
        const mapType = _.get(data, `record.field_values[map_field7].display_value`, 'roadmap').toLowerCase();

        let existingImg = mapContainer.querySelector('img');
        if(existingImg) {
            existingImg.remove();
        }

        let baseURL = "https://www.google.com/maps/embed/v1/directions?";
        iFrame.src = `${baseURL}&key=${apiKey}&${originParsed}&${destinationParsed}&${waypointsParsed}&mode=${travelMode}&units=${units}&maptype=${mapType}`;
        iFrame.style.display = "block";
        FAClient.open();
    });

    FAClient.on('openMap', ({record}) => {
        const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
        const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
        const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
        const travelDate = _.get(record, `field_values[map_field6].display_value`, '2021-05-19T07:00:00.000Z');
        const assignedTo = _.get(record, `field_values[map_field10].display_value`, 'John Smith');
        const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
        const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');

        recordId = record.id;
        FAClient.listEntityValues(
            {
                entity: "deliveries",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                deliveryLinesGlobal = lines;

                let locations = lines.map(line => {
                    return _.get(line, `field_values[deliveries_field2].value`, '');
                });

                let mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${startLocation}&zoom=10&size=600x450&maptype=roadmap&markers=color:green%7Clabel:S%7C${startLocation}&markers=color:red%7Clabel:E%7C${endLocation}&${parseWithLabel(locations.join('|'))}&scale=2&format=PNG&&key=${apiKey}`;

                let existingImg = mapContainer.querySelector('img');
                if(existingImg) {
                    existingImg.remove();
                }

                const image = document.createElement('img');
                image.src = `${mapUrl}`;
                image.width="600";
                image.height="450";
                image.addEventListener('click', async (e) => {
                    await download(mapUrl, `${assignedTo}_${travelDate}`);
                })
                mapContainer.appendChild(image);

                iFrame.src = "";
                iFrame.style.display = "none";

                FAClient.open();
            });
    });

    FAClient.on("initMap", ({record}) => {
        recordId = record.id;
        FAClient.listEntityValues(
            {
                entity: "deliveries",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                deliveryLinesGlobal = lines;
                initMap(record, lines);
            });
    })

    FAClient.on("optimise", ({record}) => {
        recordId = record.id;
        FAClient.listEntityValues(
            {
                entity: "deliveries",
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [recordId],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                deliveryLinesGlobal = lines;
                generateMap('mapPng',record, lines);
            });
    });
}



function initMap(startLocation, endLocation) {
    let markerArray = [];


    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center, // Australia.
    });

    const stepDisplay = new google.maps.InfoWindow();


    const directionsService = new google.maps.DirectionsService();
    const directionsRenderer = new google.maps.DirectionsRenderer({
        draggable: true,
        map,
        panel: document.getElementById("right-panel"),
    });
    directionsRenderer.addListener("directions_changed", () => {
        console.log(directionsRenderer.getDirections())
        computeTotalDistance(directionsRenderer.getDirections());
    });
    displayRoute(
        startLocation,
        endLocation,
        selectedWaypoints,
        directionsService,
        directionsRenderer,
        stepDisplay,
        markerArray,
        map
    );

}

function createTask() {
    if(routeResultGlobal) {
        let { geocoded_waypoints, request, routes } = routeResultGlobal;
        let legs = routes[0].legs;

        let directionsUrl = `${directionsBaseUrl}&${getPlaceIdParams(geocoded_waypoints)}&${getParamsFromLegs(legs)}`;

        console.log({ directionsUrl, geocoded_waypoints, request, legs })

        console.log(getPlaceIdParams(geocoded_waypoints))
        console.log(getParamsFromLegs(legs))

        console.log({recordsGlobal});

        let taskDescription = recordsGlobal[0]?.field_values?.logo_field3 ? `Visiting Accounts in ${recordsGlobal[0]?.field_values?.logo_field3.display_value}` : 'Visiting Accounts';

        let taskVars = {
            "entity": "task",
            "field_values": {
            "task_type": "2abf7b8b-4dbc-4aff-b981-df5daf5a06b8",
                "task_status": false,
                "overdue": false,
                "send_automatically": false,
                "send_invitations": false,
                "assigned_to": "4d53c4f2-dacd-43aa-8a2c-1e1402e7033a",
                "description": taskDescription,
                "task_field0": directionsUrl
            }
        }

        FAClient.createEntity(taskVars, ({entity_value}) => {
            console.log(entity_value);
            FAClient.showModal('entityFormModal', {
                entity: "task",
                entityLabel: 'Accounts Visit Task',
                entityInstance: entity_value,
                showButtons: false,
            });
            // FAClient.navigateTo(`/task/view/${entity_value.id}`);
        })

    }
}


function getPlaceIdParams(geocodedWaypoints) {
    let originPlaceId = "";
    let destinationPlaceId = ""
    let wayPoints = [];
    geocodedWaypoints.map((waypoint, index) => {
        if(index === 0) {
            originPlaceId = waypoint.place_id;
            return waypoint;
        }
        if(index === geocodedWaypoints.length - 1) {
            destinationPlaceId = waypoint.place_id;
            return waypoint;
        }
        wayPoints.push(waypoint.place_id);
    });

    let params = `origin_place_id=${originPlaceId}&destination_place_id=${destinationPlaceId}`;

    if(wayPoints.length > 0) {
        params = `${params}&waypoint_place_ids=${wayPoints.join('|')}`
    }
    return params;
}

function getParamsFromLegs(legs) {
    let origin = "";
    let destination = ""
    let wayPoints = [];
    legs.map((waypoint, index) => {
        if(index === 0) {
            origin = parseLocation(waypoint.start_address);
            return waypoint;
        }
        if(index === legs.length - 1) {
            destination = parseLocation(waypoint.end_address);
            if(legs.length > 2) {
                wayPoints.push(parseLocation(waypoint.start_address))
            }
            return waypoint;
        }
        wayPoints.push(parseLocation(waypoint.start_address));
    });

    let params = `origin=${origin}&destination=${destination}`;

    if(wayPoints.length > 0) {
        params = `${params}&waypoints=${wayPoints.join('|')}`
    }
    return params;
}

function displayRoute(origin, destination, waypoints, service, display, stepDisplay, markerArray, map) {
    service.route(
        {
            origin: origin,
            destination: destination,
            waypoints,
            travelMode: google.maps.TravelMode.DRIVING,
            avoidTolls: false,
            optimizeWaypoints: true
        },
        (result, status) => {
            if (status === "OK" && result) {
                console.log(result)
                routeResultGlobal = result;
                let { geocoded_waypoints, request, routes } = result;
                let legs = routes[0].legs;

                let directionsUrl = `${directionsBaseUrl}&${getPlaceIdParams(geocoded_waypoints)}&${getParamsFromLegs(legs)}`;

                console.log({ directionsUrl, geocoded_waypoints, request, legs })

                console.log(getPlaceIdParams(geocoded_waypoints))
                console.log(getParamsFromLegs(legs))

               // const waypointsParsed = `waypoints=${parseLocation(locations.join('|'))}`;
                display.setDirections(result);
                let distance = document.querySelector('#distance-container');
                let duration = document.querySelector('#duration-container');
                if(distance && duration) {
                    distance.style.display = "flex";
                    duration.style.display = "flex";
                }

                //showSteps(result, markerArray, stepDisplay, map);
            } else {
                window.alert("Directions request failed due to " + status);
            }
        }
    );
}

function computeTotalDistance(result) {
    let total = 0;
    let totalTime = 0;
    const myroute = result.routes[0];

    if (!myroute) {
        return;
    }

    for (let i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
        totalTime += myroute.legs[i].duration.value;
        console.log(myroute.legs[i])
    }
    total = total / 1000;
    totalTime = totalTime/60;
    document.getElementById("total").innerHTML = (total / 1.609).toFixed(2) + " mi";
    document.getElementById("totalDuration").innerHTML = Math.floor(totalTime) + " min";
}


function showSteps(directionResult, markerArray, stepDisplay, map) {
    // For each step, place a marker, and add the text to the marker's infowindow.
    // Also attach the marker to an array so we can keep track of it and remove it
    // when calculating new routes.
    const myRoute = directionResult.routes[0].legs[0];

    console.log(myRoute);

    for (let i = 0; i < myRoute.steps.length; i++) {
        const marker = (markerArray[i] =
            markerArray[i] || new google.maps.Marker());
        marker.setMap(map);
        marker.setPosition(myRoute.steps[i].start_location);
        attachInstructionText(
            stepDisplay,
            marker,
            myRoute.steps[i].instructions,
            map
        );
    }
}

function attachInstructionText(stepDisplay, marker, text, map) {
    google.maps.event.addListener(marker, "click", () => {
        // Open an info window when the marker is clicked on, containing the text
        // of the step.
        stepDisplay.setContent(text);
        stepDisplay.open(map, marker);
    });
}


function setMarker({address,
                   geocoder,
                   map,
                   isCenter = false,
                   icon=null,
                   title=null,
                   color = "#ea4435"
           }) {

    let accountName = title?.split('|')[1];
    let accountId = title?.split('|')[0];



    const svgMarker = {
        path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
        fillColor: color,
        fillOpacity: 1,
        strokeWeight: 0,
        rotation: 0,
        scale: 2,
        anchor: new google.maps.Point(12, 23),
    };

    if(accountName?.includes('VALE Aerospace Inc.')) {
        svgMarker.fillColor = "#00954e";
        icon = svgMarker;
    }


    const infoWindow = new google.maps.InfoWindow();

    geocoder.geocode({ address: address }, (results, status) => {
        if (status === "OK") {
            if(isCenter) {
                map.setCenter(results[0].geometry.location);
            }
            const marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location,
                icon,
                optimized: true,
            });
            // Add a click listener for each marker, and set up the info window.
            /*
            'click'
            'dblclick'
            'mouseup'
            'mousedown'
            'mouseover'
            'mouseout'
             */
            marker.addListener("click", (event) => {
                //marker.setIcon(svgMarker)

                console.log(event?.domEvent?.altKey)
                if (event?.domEvent?.altKey) {
                    FAClient.navigateTo(`/logo/view/${accountId}`)
                } else {
                    marker.setIcon(svgMarker)
                    selectedWaypoints.push({ location: `${marker.getPosition().lat()},${marker.getPosition().lng()}` })
                    console.log(marker.getPosition().lat());
                    console.log(marker.getPosition().lng());

                }
            });

            marker.addListener("dblclick", () => {
                marker.setIcon(null)
            });

            marker.addListener("mouseover", (event) => {
                infoWindow.close();
                infoWindow.setContent(accountName);
                infoWindow.open(marker.getMap(), marker);
                console.log(marker.getPosition().lat());
                console.log(marker.getPosition().lng());
            });
            marker.addListener("mouseout", () => {
                infoWindow.close();
            });
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}


/*
async function generateMap(type='mapPng', record, lines) {
    const travelMode = _.get(record, `field_values[map_field3].display_value`, 'driving').toLowerCase();
    const units = _.get(record, `field_values[map_field8].display_value`, 'metric').toLowerCase();
    const mapType = _.get(record, `field_values[map_field7].display_value`, 'roadmap').toLowerCase();
    const shiftStartTime =  _.get(record, `field_values[map_field21].display_value`, '09:00:00');
    const shiftEndTime =  _.get(record, `field_values[map_field22].display_value`, '18:00:00');
    const travelDate = _.get(record, `field_values[map_field6].display_value`, '2021-05-19T07:00:00.000Z');
    const assignedTo = _.get(record, `field_values[map_field10].display_value`, 'John Smith');
    const startLocation = _.get(record, `field_values[map_field12].display_value`, '40.6183014,-73.9985763');
    const endLocation = _.get(record, `field_values[map_field16].display_value`, '40.6183014,-73.9985763');

    const shiftStartTimeParsed = `${travelDate.split('T')[0]}T${shiftStartTime}`;
    const shiftEndTimeParsed = `${travelDate.split('T')[0]}T${shiftEndTime}`;

    let itineraryItems = lines.map(line => {
        const priority = _.get(line, `field_values[deliveries_field7].value`, '1');
        const latLonLocation = _.get(line, `field_values[deliveries_field2].value`, '40.6183014,-73.9985763');
        return {
            "openingTime": shiftStartTimeParsed,
            "closingTime": shiftEndTimeParsed,
            "dwellTime": "00:00:01.00",
            "priority": parseFloat(priority),
            "location": getLocation(latLonLocation)
        }
    });



    let postDataSample = {
        "agents": [
            {
                "name": assignedTo,
                "shifts": [
                    {
                        "startTime": shiftStartTimeParsed,
                        "startLocation": getLocation(startLocation),
                        "endTime": shiftEndTimeParsed,
                        "endLocation": getLocation(endLocation)
                    }
                ]
            }
        ],
        itineraryItems
    };

    console.log({postDataSample, record, lines});

    postData('https://dev.virtualearth.net/REST/V1/Routes/OptimizeItinerary?key=ApG0F4UDDJImvZJw-YRhOf0wTEUWQ3O-v4mx7ua0hiYOUhHB43UR3CEoCVc_zAa4', postDataSample)
        .then(async ({ resourceSets, statusCode }) => {
            let agentItinerary = _.get(resourceSets, `[0].resources[0].agentItineraries[0].route.wayPoints`);
            let route = _.get(resourceSets, `[0].resources[0].agentItineraries[0].route`);
            let instructions = _.get(resourceSets, `[0].resources[0].agentItineraries[0].instructions`);

            let parseDuration = (duartionStr) => {
                let durationSplit = duartionStr.split(':');
                let hours = parseFloat(durationSplit[0]) * 3600;
                let min = parseFloat(durationSplit[1]) * 60;
                let sec = parseFloat(durationSplit[2]);
                return hours + min + sec;
            }

            var et = new Date(route.endTime).addHours(5);

            let fieldValues = {
                map_field23: route.totalTravelDistance / 1000,
                map_field9: parseDuration(route.totalTravelTime),
                map_field24: et.toISOString()
            }


            let sortedLines = [];

            let linesObj = {};
            deliveryLinesGlobal.map(line => {
                const latLonLocation = _.get(line, `field_values[deliveries_field2].value`);
                if(latLonLocation) {
                    linesObj[latLonLocation] = line;
                    updateRecord(line.id, 'deliveries', { deleted : true })
                }
            })

            let locationPins = [];
            let order = 1;
            for (let index = 0; index < instructions.length; index++) {
                let {instructionType, itineraryItem} = instructions[index];
                if (instructionType === "VisitLocation") {
                    let latLon = `${itineraryItem.location.latitude},${itineraryItem.location.longitude}`;
                    locationPins.push(latLon);
                    let travelInfo = instructions[index - 1];
                    let travelEndTime = travelInfo.endTime;
                    let travelDuration = parseDuration(travelInfo.duration);
                    let travelDistance = travelInfo.distance / 1000;
                    let package = _.get(linesObj[latLon], `field_values[deliveries_field0].value`)
                    var et = new Date(travelEndTime).addHours(5);
                    let lineFields = {
                        deliveries_field9: et,
                        deliveries_field10: travelDistance,
                        deliveries_field11: order,
                        deliveries_field12: travelDuration,
                        deliveries_field0: package,
                        parent_entity_reference_id: record.id
                    }
                    //updateRecord(linesObj[latLon].id, 'deliveries', { deleted : true })
                    //updateRecord(linesObj[latLon].id, 'deliveries',lineFields)
                    console.log(lineFields);
                    FAClient.createEntity({entity: "deliveries", field_values: lineFields}, (data) => {
                        console.log(data);
                    })

                    sortedLines.push({lines: linesObj[latLon], itineraryItem});
                    order++;
                }
            }
            updateRecord(record.id, 'map',fieldValues);
            order = 1;


                agentItinerary.map((point, index) => {
                   let latLon = `${point.latitude},${point.longitude}`;
                   locationPins.push(latLon);
                })



            let mapUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${startLocation}&zoom=10&size=600x450&maptype=roadmap&markers=color:green%7Clabel:S%7C${startLocation}&markers=color:red%7Clabel:E%7C${endLocation}&${parseWithLabel(locationPins.join('|'))}&scale=2&format=PNG&&key=${apiKey}`;

            const iFrame = document.querySelector('#directions-iframe');
            const mapContainer = document.querySelector('#map-container');
            console.log({sortedLines});
            let existingImg = mapContainer.querySelector('img');
            if(existingImg) {
                existingImg.remove();
            }

            const image = document.createElement('img');
            image.src = `${mapUrl}`;
            image.width="600";
            image.height="450";
            image.addEventListener('click', async (e) => {
                await download(mapUrl, `${assignedTo}_${travelDate}`);
            })

            mapContainer.appendChild(image);

            iFrame.src = "";
            iFrame.style.display = "none";

            FAClient.open();
        });
    return 'Ok';
}

 */

/*
const tourStops = [
    [{ lat: 34.8791806, lng: -111.8265049 }, "Boynton Pass"],
    [{ lat: 34.8559195, lng: -111.7988186 }, "Airport Mesa"],
    [{ lat: 34.832149, lng: -111.7695277 }, "Chapel of the Holy Cross"],
    [{ lat: 34.823736, lng: -111.8001857 }, "Red Rock Crossing"],
    [{ lat: 34.800326, lng: -111.7665047 }, "Bell Rock"],
];
// Create an info window to share between markers.
const infoWindow = new google.maps.InfoWindow();
// Create the markers.
tourStops.forEach(([position, title], i) => {
    const marker = new google.maps.Marker({
        position,
        map,
        title: `${i + 1}. ${title}`,
        label: `${i + 1}`,
        optimized: false,
    });
    // Add a click listener for each marker, and set up the info window.
    marker.addListener("click", () => {
        infoWindow.close();
        infoWindow.setContent(marker.getTitle());
        infoWindow.open(marker.getMap(), marker);
    });
});

 */
